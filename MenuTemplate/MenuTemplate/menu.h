/*
 * menu.h
 *
 * Created: 25.02.2021 23:17:17
 *  Author: Ja
 */ 


#ifndef MENU_H_
#define MENU_H_

typedef enum 
{
  None = 0,
  Main,
  TypeMeasure,
  LimitMeasure,
  ClearHistory,
  RelativeMeasure,
  AbsoluteMeasure,  
}menu_screen;

typedef struct
{
  uint8_t STATE;
  uint16_t max;
  uint16_t min;
  uint16_t index;
}menu_variable;

typedef struct
{
	uint8_t butOK;
	uint8_t butUp;
	uint8_t butDown;
	uint8_t butRet;
	unsigned long butOK_blocktime;
	unsigned long butUp_blocktime;
	unsigned long butDown_blocktime;
	unsigned long butRet_blocktime;
}but_state;

extern menu_screen actualMenuScreen;

void menuInit();

void menuRun(uint8_t butUp,uint8_t butDown,uint8_t butOK,uint8_t buRet);


#endif /* MENU_H_ */