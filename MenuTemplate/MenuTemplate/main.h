/*
 * main.h
 *
 * Created: 25.02.2021 23:17:17
 *  Author: Ja
 */ 


#ifndef MAIN_H_
#define MAIN_H_

#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SH1106.h>
#include <VL53L1X.h>

extern Adafruit_SH1106 display;


#endif /* MAIN_H_ */