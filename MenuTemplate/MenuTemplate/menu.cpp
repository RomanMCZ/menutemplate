/*
 * menu.cpp
 *
 * Created: 25.02.2021 23:17:00
 *  Author: Ja
 */ 

#include <stdint.h>
#include "main.h"
#include "menu.h"

#define ROW_INVERTED display.setTextColor(BLACK, WHITE)
#define ROW_NORMAL display.setTextColor(WHITE,BLACK)

menu_screen actualMenuScreen = None;
menu_variable actualScreenVar;
but_state butState;

uint8_t butOKstate;
uint8_t butUpstate;
uint8_t butDownstate;
uint8_t butRetstate;

void Menu_Setting(uint8_t butUp,uint8_t butDown,uint8_t butOK)
{
	if(actualScreenVar.STATE == 0)
	{		
		actualScreenVar.min = 1;  // omezen� po�tu polo�ek v menu
		actualScreenVar.max = 4;  // omezen� po�tu polo�ek v menu
		actualScreenVar.index = 1;
		actualScreenVar.STATE = 1;
		display.clearDisplay();
	}
	
	//Zpracovani tlacitek
	if(butUp)
		if(actualScreenVar.index < actualScreenVar.max)
			actualScreenVar.index++;
	
	if(butDown)
		if(actualScreenVar.index > actualScreenVar.min)
			actualScreenVar.index--;
	
	if(butOK) 
	{
		actualScreenVar.STATE = 0;
		switch(actualScreenVar.index)
		{
			case  1 : actualMenuScreen = TypeMeasure; break;
			case  2 : actualMenuScreen = LimitMeasure; break;
			case  3 : actualMenuScreen = ClearHistory; break;
			case  4 : actualMenuScreen = None; break;
			default:
				break;
		}
	}
	
	ROW_NORMAL;
	display.setTextSize(1);
	display.setCursor(2, 0);	
	display.println("Nastaveni");
	display.drawLine(0, 9, 128, 9, WHITE);		
	
	(actualScreenVar.index == 1 ? ROW_INVERTED : ROW_NORMAL); display.setCursor (2, 15); display.println("Typ mereni");
	(actualScreenVar.index == 2 ? ROW_INVERTED : ROW_NORMAL); display.setCursor (2, 28); display.println("Hranice mereni");
	(actualScreenVar.index == 3 ? ROW_INVERTED : ROW_NORMAL); display.setCursor (2, 41); display.println("Vymazani historie");
	(actualScreenVar.index == 4 ? ROW_INVERTED : ROW_NORMAL); display.setCursor (2, 54); display.println("Zpet");
	display.display();	
}

void Menu_typeMeasure(uint8_t butUp,uint8_t butDown,uint8_t butOK)
{
	if(actualScreenVar.STATE == 0)
	{
		actualScreenVar.min = 1;
		actualScreenVar.max = 3;
		actualScreenVar.index = 1;
		actualScreenVar.STATE = 1;
		display.clearDisplay();
	}
	
	//Zpracovani tlacitek
	if(butUp)
		if(actualScreenVar.index < actualScreenVar.max)
			actualScreenVar.index++;
	
	if(butDown)
		if(actualScreenVar.index > actualScreenVar.min)
			actualScreenVar.index--;
	
	if(butOK)
	{
		actualScreenVar.STATE = 0;
		switch(actualScreenVar.index)
		{
			case  1 : actualMenuScreen = AbsoluteMeasure; break;
			case  2 : actualMenuScreen = RelativeMeasure; break;
			case  3 : actualMenuScreen = None; break;
			default:
			break;
		}
	}
	
	ROW_NORMAL;
	display.setTextSize(1);
	display.setCursor(2, 0);
	display.println("Typ mereni");
	display.drawLine(0, 9, 128, 9, WHITE);
	
	(actualScreenVar.index == 1 ? ROW_INVERTED : ROW_NORMAL); display.setCursor (2, 15); display.println("Absolutni");
	(actualScreenVar.index == 2 ? ROW_INVERTED : ROW_NORMAL); display.setCursor (2, 28); display.println("Relativni");
	(actualScreenVar.index == 3 ? ROW_INVERTED : ROW_NORMAL); display.setCursor (2, 41); display.println("Zpet");
	display.display();
}

void Menu_relativeMeasure(uint8_t butUp,uint8_t butDown,uint8_t butOK)
{
	if(actualScreenVar.STATE == 0)
	{
		actualScreenVar.min = 1;
		actualScreenVar.max = 3;
		actualScreenVar.index = 1;
		actualScreenVar.STATE = 1;
		display.clearDisplay();
	}
	
	//Zpracovani tlacitek
	if(butUp)
		if(actualScreenVar.index < actualScreenVar.max)
			actualScreenVar.index++;
	
	if(butDown)
		if(actualScreenVar.index > actualScreenVar.min)
			actualScreenVar.index--;
	
	if(butOK)
	{
		actualScreenVar.STATE = 0;
		actualMenuScreen = TypeMeasure;
	}
	
	display.setTextSize(1);
	display.setTextColor(WHITE);
	display.setCursor(2, 0);
	display.println("Relativni");
	display.drawLine(0, 9, 128, 9, WHITE);     // display.drawLine(startX, startY, endX, endY, color)
	
	display.setTextSize(1);
	display.setTextColor(BLACK, WHITE);
	display.setCursor (7, 15);
	display.println(" Referencni mereni ");
	
	display.setTextSize(1);
	display.setTextColor(WHITE);
	display.setCursor (2, 32);
	display.println("Ultrazvuk:");
	
	display.setTextSize(1);
	display.setCursor (2, 47);
	display.println("Laser:");
	
	display.display();
}

void Menu_clearHistory(uint8_t butUp,uint8_t butDown,uint8_t butOK)
{
	if(actualScreenVar.STATE == 0)
	{
		actualScreenVar.min = 1;
		actualScreenVar.max = 3;
		actualScreenVar.index = 1;
		actualScreenVar.STATE = 1;
		display.clearDisplay();
	}
	
	//Zpracovani tlacitek
	if(butUp)
		if(actualScreenVar.index < actualScreenVar.max)
			actualScreenVar.index++;
	
	if(butDown)
		if(actualScreenVar.index > actualScreenVar.min)
			actualScreenVar.index--;
	
	if(butOK)
	{
		actualScreenVar.STATE = 0;
		actualMenuScreen = Main;
	}
	
	display.setTextSize(1);
	display.setTextColor(WHITE);
	display.setCursor(2, 0);
	display.println("Vymazani historie");
	display.drawLine(0, 9, 128, 9, WHITE);     // display.drawLine(startX, startY, endX, endY, color)
	
	display.setCursor (2, 15);
	display.println("Opravdu vymazat?");
	
	(actualScreenVar.index == 1 ? ROW_INVERTED : ROW_NORMAL); display.setCursor (2, 31); display.println("Ano");
	(actualScreenVar.index == 2 ? ROW_INVERTED : ROW_NORMAL); display.setCursor (2, 44); display.println("Ne");
	display.display();
}

void Menu_limitMeasure(uint8_t butUp,uint8_t butDown,uint8_t butOK)
{
	if(actualScreenVar.STATE == 0)
	{
		actualScreenVar.index = 0;
		actualScreenVar.STATE = 1;
		display.clearDisplay();
	}
	
	if(actualScreenVar.STATE == 1)
	{
		actualScreenVar.min = 0;
		actualScreenVar.max = 9000;
		
		//Zpracovani tlacitek
		if(butUp)
			if(actualScreenVar.index < actualScreenVar.max)
				actualScreenVar.index += 1000;
		
		if(butDown)
			if(actualScreenVar.index > actualScreenVar.min)
				actualScreenVar.index -= 1000;
		
		if(butOK)
		{
			actualScreenVar.STATE = 2;
		}
		
		display.setTextSize(1);
		display.setTextColor(WHITE);
		display.setCursor(2, 0);
		display.println("Hranice mereni");
		display.drawLine(0, 9, 128, 9, WHITE);
		
		//LCD_print("****** Hodnota *********");
		//LCD_print("       *                ");
		//LCD_print("       0 0 0 0          ");
		//LCD_print("       ^                ");
	}
	
	if(actualScreenVar.STATE == 2)
	{
		actualScreenVar.min = 0;
		actualScreenVar.max = 900;
		
		//Zpracovani tlacitek
		if(butUp)
			if(actualScreenVar.index < actualScreenVar.max)
				actualScreenVar.index += 100;
		
		if(butDown)
			if(actualScreenVar.index > actualScreenVar.min)
				actualScreenVar.index -= 100;
		
		if(butOK)
		{
			actualScreenVar.STATE = 3;
		}
		
		display.setTextSize(1);
		display.setTextColor(WHITE);
		display.setCursor(2, 0);
		display.println("Hranice mereni");
		display.drawLine(0, 9, 128, 9, WHITE);
				
		//LCD_print("****** Hodnota *********");
		//LCD_print("         *              ");
		//LCD_print("       0 0 0 0          ");
		//LCD_print("         ^              ");
	}
	
	if(actualScreenVar.STATE == 3)
	{
		actualScreenVar.min = 0;
		actualScreenVar.max = 90;
		
		//Zpracovani tlacitek
		if(butUp)
			if(actualScreenVar.index < actualScreenVar.max)
				actualScreenVar.index += 10;
		
		if(butDown)
			if(actualScreenVar.index > actualScreenVar.min)
				actualScreenVar.index -= 10;
		
		if(butOK)
		{
			actualScreenVar.STATE = 4;
		}
		
		display.setTextSize(1);
		display.setTextColor(WHITE);
		display.setCursor(2, 0);
		display.println("Hranice mereni");
		display.drawLine(0, 9, 128, 9, WHITE);
		
		//LCD_print("****** Hodnota *********");
		//LCD_print("           *            ");
		//LCD_print("       0 0 0 0          ");
		//LCD_print("           ^            ");
	}

	if(actualScreenVar.STATE == 4)
	{
		actualScreenVar.min = 0;
		actualScreenVar.max = 9;
		
		//Zpracovani tlacitek
		if(butUp)
			if(actualScreenVar.index < actualScreenVar.max)
				actualScreenVar.index += 1;
		
		if(butDown)
			if(actualScreenVar.index > actualScreenVar.min)
				actualScreenVar.index -= 1;
		
		if(butOK)
		{
			actualScreenVar.STATE = 0;
			actualMenuScreen = Main;
			
			//promenna = actualScreenVar.index
		}
		
		display.setTextSize(1);
		display.setTextColor(WHITE);
		display.setCursor(2, 0);
		display.println("Hranice mereni");
		display.drawLine(0, 9, 128, 9, WHITE);
		
		//LCD_print("****** Hodnota *********");
		//LCD_print("             *          ");
		//LCD_print("       0 0 0 0          ");
		//LCD_print("             ^          ");
	}
	
	display.display();
}

void menuInit()
{
	actualMenuScreen = None;
	actualScreenVar.STATE = 0;
	actualScreenVar.max = 0;
	actualScreenVar.min = 0;
	actualScreenVar.index = 0;
}

void menuRun(uint8_t butUp,uint8_t butDown,uint8_t butOK,uint8_t butRet)
{	
	
	butOKstate = (butState.butOK == butOK ? 0 :  butOK);
	butUpstate = (butState.butUp == butUp ? 0 :  butUp);
	butDownstate = (butState.butDown == butDown ? 0 : butDown);
	butRetstate = (butState.butRet == butRet ? 0 :  butRet);
	
	switch (actualMenuScreen)
	{
		case Main: Menu_Setting(butUpstate,butDownstate,butOKstate); break;
		case TypeMeasure: Menu_typeMeasure(butUpstate,butDownstate,butOKstate); break;
		case LimitMeasure: Menu_limitMeasure(butUpstate,butDownstate,butOKstate); break;
		case ClearHistory: Menu_clearHistory(butUpstate,butDownstate,butOKstate); break;
		case RelativeMeasure: Menu_relativeMeasure(butUpstate,butDownstate,butOKstate); break;
		default:
		break;
	}
	
	if( butState.butOK && !butOK && (abs(butState.butOK_blocktime - millis()) >= 500) )
	{
		butState.butOK = butOK;
		butState.butOK_blocktime = millis();
	}
	else if(!butState.butOK && butOK)
		butState.butOK = butOK;
	
	if( butState.butDown && !butDown && (abs(butState.butDown_blocktime - millis()) >= 500) )
	{
		butState.butDown = butDown;
		butState.butDown_blocktime = millis();
	}
	else if(!butState.butDown && butDown)
		butState.butDown = butDown;

	if( butState.butUp && !butUp && (abs(butState.butUp_blocktime - millis()) >= 500) )
	{
		butState.butUp = butUp;
		butState.butUp_blocktime = millis();
	}
	else if(!butState.butUp && butUp)
		butState.butUp = butUp;
	
	if( butState.butRet && !butRet && (abs(butState.butRet_blocktime - millis()) >= 500) )
	{
		butState.butRet = butRet;
		butState.butRet_blocktime = millis();
	}
	else if(!butState.butRet && butRet)
		butState.butRet = butRet;
}
